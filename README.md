# FileSync
#### FileSync是基于SSH互信的Linux服务器之间的文件或文件夹的单向的、准实时（秒级）的同步方案，通过inotifywait监听源文件/目录的变化，并通过rsync实现文件批量同步至指定linux服务器，实现文件实时同步;
> 特点:
* 支持一对一和一对多同步；
* 支持多线程同步；
* 支持同步至多个目标服务器；
* 支持失败重传;
* 支持周期全量核查和同步；
* 支持动态启停控制；
* 支持热加载同步列表;
* 支持自由控制inotify的监听事件（如move、create、delete、attrib等）；
* 支持自由控制rsync的传输参数（如压缩、校验、过滤规则等）；

### 流程框架
<img src="Docs/文件同步流程框架.jpg" width="1000"/>

### 贡献
Bingo 

### 许可证
[Apache License 2.0](LICENSE)

